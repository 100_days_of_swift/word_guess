//
//  ViewController.swift
//  WordGuess
//
//  Created by Hariharan S on 18/05/24.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Properties

    var buttonsView: UIView!
    var scoreLabel: UILabel!
    var cluesLabel: UILabel!
    var answersLabel: UILabel!
    var clearButton: UIButton!
    var submitButton: UIButton!
    var currentAnswer: UITextField!
    var letterButtons = [UIButton]()
    
    var level = 1
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    
    var solutions = [String]()
    var activatedButtons = [UIButton]()

    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = .white

        self.scoreLabel = UILabel()
        self.scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        self.scoreLabel.textAlignment = .right
        self.scoreLabel.text = "Score: 0"
        self.view.addSubview(self.scoreLabel)
        
        self.cluesLabel = UILabel()
        self.cluesLabel.translatesAutoresizingMaskIntoConstraints = false
        self.cluesLabel.font = UIFont.systemFont(ofSize: 24)
        self.cluesLabel.text = "CLUES"
        self.cluesLabel.numberOfLines = 0
        self.view.addSubview(self.cluesLabel)

        self.answersLabel = UILabel()
        self.answersLabel.translatesAutoresizingMaskIntoConstraints = false
        self.answersLabel.font = UIFont.systemFont(ofSize: 24)
        self.answersLabel.text = "ANSWERS"
        self.self.answersLabel.numberOfLines = 0
        self.answersLabel.textAlignment = .right
        self.view.addSubview(self.answersLabel)
        
        self.currentAnswer = UITextField()
        self.currentAnswer.translatesAutoresizingMaskIntoConstraints = false
        self.currentAnswer.placeholder = "Tap letters to guess"
        self.currentAnswer.textAlignment = .center
        self.currentAnswer.font = UIFont.systemFont(ofSize: 44)
        self.currentAnswer.isUserInteractionEnabled = false
        self.view.addSubview(self.currentAnswer)
        
        self.submitButton = UIButton(type: .system)
        self.submitButton.translatesAutoresizingMaskIntoConstraints = false
        self.submitButton.setTitle("SUBMIT", for: .normal)
        self.submitButton.addTarget(
            self,
            action: #selector(self.submitTapped(_:)),
            for: .touchUpInside
        )
        self.view.addSubview(self.submitButton)

        self.clearButton = UIButton(type: .system)
        self.clearButton.translatesAutoresizingMaskIntoConstraints = false
        self.clearButton.setTitle("CLEAR", for: .normal)
        self.clearButton.addTarget(
            self,
            action: #selector(self.clearTapped(_:)),
            for: .touchUpInside
        )
        self.view.addSubview(self.clearButton)
        
        self.buttonsView = UIView()
        self.buttonsView.layer.borderWidth = 1
        self.buttonsView.layer.cornerRadius = 16.0
        self.buttonsView.layer.borderColor = UIColor.gray.cgColor
        self.buttonsView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.buttonsView)
        
        self.setUpConstraints()
        
        self.cluesLabel.setContentHuggingPriority(UILayoutPriority(1), for: .vertical)
        self.answersLabel.setContentHuggingPriority(UILayoutPriority(1), for: .vertical)
        
        self.setAnswerButtons()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadLevel()
    }
}

// MARK: - Private Methods

private extension ViewController {
    func setUpConstraints() {
        NSLayoutConstraint.activate([
            self.scoreLabel.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor),
            self.scoreLabel.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
            
            self.cluesLabel.topAnchor.constraint(equalTo: self.scoreLabel.bottomAnchor),
            self.cluesLabel.leadingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.leadingAnchor,
                constant: 100
            ),
            self.cluesLabel.widthAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.widthAnchor,
                multiplier: 0.6,
                constant: -100
            ),
            
            self.answersLabel.topAnchor.constraint(equalTo: self.scoreLabel.bottomAnchor),
            self.answersLabel.trailingAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.trailingAnchor,
                constant: -100
            ),
            self.answersLabel.widthAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.widthAnchor,
                multiplier: 0.4,
                constant: -100
            ),
            self.answersLabel.heightAnchor.constraint(equalTo: self.cluesLabel.heightAnchor),
            
            self.currentAnswer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.currentAnswer.widthAnchor.constraint(
                equalTo: self.view.widthAnchor,
                multiplier: 0.5
            ),
            self.currentAnswer.topAnchor.constraint(
                equalTo: self.cluesLabel.bottomAnchor,
                constant: 20
            ),
            
            self.submitButton.topAnchor.constraint(equalTo: self.currentAnswer.bottomAnchor),
            self.submitButton.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor,
                constant: -100
            ),
            self.submitButton.heightAnchor.constraint(equalToConstant: 44),

            self.clearButton.centerXAnchor.constraint(
                equalTo: self.view.centerXAnchor,
                constant: 100
            ),
            self.clearButton.centerYAnchor.constraint(equalTo: self.submitButton.centerYAnchor),
            self.clearButton.heightAnchor.constraint(equalToConstant: 44),
            
            self.buttonsView.widthAnchor.constraint(equalToConstant: 750),
            self.buttonsView.heightAnchor.constraint(equalToConstant: 320),
            self.buttonsView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.buttonsView.topAnchor.constraint(
                equalTo: self.submitButton.bottomAnchor,
                constant: 20
            ),
            self.buttonsView.bottomAnchor.constraint(
                equalTo: self.view.layoutMarginsGuide.bottomAnchor,
                constant: -20
            )
        ])
    }
    
    func setAnswerButtons() {
        let width = 150
        let height = 80

        for row in 0..<4 {
            for col in 0..<5 {
                let letterButton = UIButton(type: .system)
                letterButton.titleLabel?.font = UIFont.systemFont(ofSize: 36)
                letterButton.setTitle("WWW", for: .normal)
                let frame = CGRect(
                    x: col * width,
                    y: row * height,
                    width: width,
                    height: height
                )
                letterButton.frame = frame
                letterButton.addTarget(
                    self,
                    action: #selector(self.letterTapped(_:)),
                    for: .touchUpInside
                )
                self.buttonsView.addSubview(letterButton)
                self.letterButtons.append(letterButton)
            }
        }
    }
    
    func loadLevel() {
        var clueString = ""
        var solutionString = ""
        var letterBits = [String]()

        if let levelFileURL = Bundle.main.url(
            forResource: "level\(self.level)",
            withExtension: "txt"
        ) {
            if let levelContents = try? String(contentsOf: levelFileURL) {
                var lines = levelContents.components(separatedBy: "\n")
                lines.shuffle()

                for (index, line) in lines.enumerated() {
                    let parts = line.components(separatedBy: ": ")
                    let answer = parts[0]
                    let clue = parts[1]

                    clueString += "\(index + 1). \(clue)\n"

                    let solutionWord = answer.replacingOccurrences(of: "|", with: "")
                    solutionString += "\(solutionWord.count) letters\n"
                    self.solutions.append(solutionWord)

                    let bits = answer.components(separatedBy: "|")
                    letterBits += bits
                }
            }
        }

        self.cluesLabel.text = clueString.trimmingCharacters(in: .whitespacesAndNewlines)
        self.answersLabel.text = solutionString.trimmingCharacters(in: .whitespacesAndNewlines)

        letterBits.shuffle()

        if letterBits.count == self.letterButtons.count {
            for i in 0 ..< self.letterButtons.count {
                self.letterButtons[i].setTitle(
                    letterBits[i],
                    for: .normal
                )
            }
        }
    }
    
    func levelUp(action: UIAlertAction) {
        self.level += 1
        self.solutions.removeAll(keepingCapacity: true)

        self.loadLevel()

        for btn in self.letterButtons {
            btn.isHidden = false
        }
    }
}

// MARK: - Objc Methods

@objc
private extension ViewController {
    func letterTapped(_ sender: UIButton) {
        guard let buttonTitle = sender.titleLabel?.text 
        else {
            return
        }
        self.currentAnswer.text = self.currentAnswer.text?.appending(buttonTitle)
        self.activatedButtons.append(sender)
        sender.isHidden = true
    }

    func submitTapped(_ sender: UIButton) {
        guard let answerText = self.currentAnswer.text
        else {
            return
        }

        if let solutionPosition = self.solutions.firstIndex(of: answerText) {
            self.activatedButtons.removeAll()

            var splitAnswers = self.answersLabel.text?.components(separatedBy: "\n")
            splitAnswers?[solutionPosition] = answerText
            self.answersLabel.text = splitAnswers?.joined(separator: "\n")

            self.currentAnswer.text = ""
            self.score += 1

            if self.score % 7 == 0 {
                let ac = UIAlertController(
                    title: "Well done!",
                    message: "Are you ready for the next level?",
                    preferredStyle: .alert
                )
                ac.addAction(
                    UIAlertAction(
                        title: "Let's go!",
                        style: .default,
                        handler: self.levelUp
                    )
                )
                self.present(ac, animated: true)
            }
        } else {
            let ac = UIAlertController(
                title: "Oops!, Wrong Guess",
                message: nil,
                preferredStyle: .alert
            )
            ac.addAction(
                UIAlertAction(
                    title: "OK",
                    style: .default
                )
            )
            self.present(ac, animated: true)
        }
    }

    func clearTapped(_ sender: UIButton) {
        self.currentAnswer.text = ""
        for btn in self.activatedButtons {
            btn.isHidden = false
        }
        self.activatedButtons.removeAll()
    }
}
